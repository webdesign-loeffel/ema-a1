package com.example.lucas.aufgabe1;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Spinner;
import android.widget.ArrayAdapter;

public class MainActivity extends AppCompatActivity{

    Spinner spinner;
    Spinner spinner_color;
    TextView resText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Spinner
        spinner = (Spinner) findViewById(R.id.fontSizeSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.fontSizeArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        resText = (TextView)findViewById(R.id.resText);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int spinnerPosition = spinner.getSelectedItemPosition();
                switch (spinnerPosition) {
                    case 0:
                        resText.setTextSize(24);
                        break;
                    case 1:
                        resText.setTextSize(30);
                        break;
                    case 2:
                        resText.setTextSize(36);
                        break;
                    default:
                        resText.setTextSize(20);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Color Spinner
        spinner_color = (Spinner) findViewById(R.id.fontColorSpinner);
        ArrayAdapter<CharSequence> color_adapter = ArrayAdapter.createFromResource(this, R.array.fontColorArray, android.R.layout.simple_spinner_item);
        color_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_color.setAdapter(color_adapter);
        resText = (TextView)findViewById(R.id.resText);
        spinner_color.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int spinner_color_Position = spinner_color.getSelectedItemPosition();
                switch (spinner_color_Position) {
                    case 0:
                        resText.setTextColor(Color.parseColor("#000000"));
                        break;
                    case 1:
                        resText.setTextColor(Color.parseColor("#FF0000")); //red
                        break;
                    case 2:
                        resText.setTextColor(Color.parseColor("#0000FF")); //blue
                        break;
                    default:
                        resText.setTextColor(Color.parseColor("#00FF00")); //green
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void mainClickButton(View view){
        EditText mainText = (EditText)findViewById(R.id.mainText);
        String mainTextStr = mainText.getText().toString().trim();
        resText = (TextView)findViewById(R.id.resText);
        resText.setText(mainTextStr);
    }
}
